import itertools
from typing import List
from PIL import Image


def get_rgb_hash(source: str) -> List:
    def add_with_max(init: int, val: int) -> int: return init+val if init+val < 256 else init+val-256
    it = zip((b for b in source.encode()), itertools.cycle([0, 1, 2]))
    start = [0, 0, 0]
    for b, c in it: start[c] = add_with_max(start[c], b)
    return tuple(start)


def get_rgb_hash_image(source: str):
    return Image.new("RGB", (250, 50), get_rgb_hash(source))
